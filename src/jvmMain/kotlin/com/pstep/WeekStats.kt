package com.pstep

data class WeekStats (
    val previous_week_goal: Int,
    val minutes_achieved_previous_week: Int,
)